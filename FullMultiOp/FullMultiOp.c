#include "stdafx.h"
#include "TestList.h"
#include "csprng.h"

void ListDirectoryFiles(TCHAR *directory, List* fileList, List *directoryList) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[100];
	wcscpy(searchString, directory);
	wcscat(searchString, L"*");
	TCHAR filepath[MAX_PATH];

	handle = FindFirstFile(searchString, &file);
	searchString[wcslen(searchString) - 1] = 0;

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			wcscpy(filepath, directory);
			wcscat(filepath, file.cFileName);
			if (wcsstr(file.cFileName, L".txt") == NULL)
				prepend_list(directoryList, &filepath);
			else
				prepend_list(fileList, &filepath);
		}
	}
}

TCHAR *randstring(size_t length, BOOL truRand) {
	static char charset[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	TCHAR *randomString = NULL;
	CSPRNG rng2 = NULL;
	rng2 = csprng_create(rng2);
	if (!rng2)
		return 1;

	if (length) {
		randomString = malloc(sizeof(TCHAR) * (length + 1) + 10);
		if (randomString) {
			for (int n = 0; n < length; n++) {
				int key;
				if (truRand == TRUE) {
					key = csprng_get_int(rng2);
					key %= (int)(sizeof(charset) - 1);
				}
				else
					key = rand() % (int)(sizeof(charset) - 1);

				randomString[n] = charset[key];
			}
			randomString[length] = '\0';
		}
	}
	return randomString;
}


/*
CREATE 0-20		20
READ 21-70		50
UPDATE 71-105	35
MOVE 106-115	10
DELETE 116-120	5
*/

void fileOperation(List *fileList, TCHAR *base) {
	FILE *fp;
	TCHAR toUse[MAX_PATH * 2], toRename[MAX_PATH * 2], *random, combined[MAX_PATH];
	char usablePath[MAX_PATH * 2], newPath[MAX_PATH * 2];
	int fileOp;

	int myInt = rand() % list_size(fileList);
	ListNode *tmp = select_node(fileList, myInt);
	wcscpy(toUse, tmp->filename);
	fileOp = rand() % 121;
	memset(usablePath, 0, MAX_PATH);

	if (fileOp >= 0 && fileOp <= 20) {
		wcscpy(combined, base);
		random = randstring(80, FALSE);
		wcscat(random, L".txt");
		wcscat(combined, random);
		wcstombs(usablePath, combined, MAX_PATH);
		fp = fopen(usablePath, "w+");
		fclose(fp);
		free(random);
	}
	else if (fileOp >= 21 && fileOp <= 70) {
		wcstombs(usablePath, toUse, MAX_PATH);
		fp = fopen(usablePath, "r");
		if (fp != NULL) {
			char buffer[1000];
			fread(buffer, 1000, 1, fp);
			fclose(fp);
		}
	}
	else if (fileOp >= 71 && fileOp <= 105) {
		wcstombs(usablePath, toUse, MAX_PATH);
		fp = fopen(usablePath, "a+");
		random = randstring(100, FALSE);
		fprintf(fp, "%ws\r\n", random);
		fclose(fp);
		free(random);
	}
	else if (fileOp >= 106 && fileOp <= 115) {
		wcscpy(toRename, base);
		random = randstring(80, FALSE);
		wcscat(toRename, random);
		wcscat(toRename, L".txt");
		MoveFile(toUse, toRename);
		wcscpy(tmp->filename, toRename);
		free(random);
	}
	else {
		DeleteFile(tmp->filename);
		remove_any(fileList, tmp);
	}
}

/*
CREATE 0-29		30
RENAME 30		1
DELETE 31		1
*/

void deleteDirectory(TCHAR *directory) {
	HANDLE handle;
	WIN32_FIND_DATA file;

	TCHAR searchString[200];
	wcscpy(searchString, directory);
	wcscat(searchString, L"\\*");
	TCHAR filepath[MAX_PATH * 2];

	handle = FindFirstFile(searchString, &file);

	if (handle != INVALID_HANDLE_VALUE) {
		FindNextFile(handle, &file);
		while (FindNextFile(handle, &file) != 0) {
			memset(filepath, 0, MAX_PATH * 2);
			wcscpy(filepath, directory);
			wcscat(filepath, "\\");
			wcscat(filepath, file.cFileName);
			DeleteFile(filepath);
		}
	}
}

void directoryOperation(List *directoryList, TCHAR *baseDir) {
	int fileOp;
	FILE *fp;
	TCHAR toRename[MAX_PATH * 2], *random, combined[MAX_PATH * 2], tNewPath[MAX_PATH * 2];
	char usablePath[MAX_PATH * 2], newPath[MAX_PATH * 2], oldPath[MAX_PATH * 2];
	int error, counter = 0;
	BOOL success = FALSE;

	int myInt = rand() % list_size(directoryList);
	ListNode *tmp = select_node(directoryList, myInt);
	wcstombs(oldPath, tmp->filename, MAX_PATH);
	fileOp = rand() % 32;

	if (fileOp >= 0 && fileOp <= 29) {
		
		wcscpy(combined, baseDir);
		random = randstring(80, FALSE);
		wcscat(combined, random);
		CreateDirectory(combined, NULL);
		free(random);
	}
	else if (fileOp == 30) {
		
		wcscpy(tNewPath, baseDir);
		random = randstring(80, FALSE);
		wcscat(tNewPath, random);
		success = MoveFileW(tmp->filename, tNewPath, NULL);
		wcscpy(tNewPath, tmp->filename);
		free(random);
	}
	else if (fileOp == 31) {
		deleteDirectory(tmp->filename);
		success = RemoveDirectory(tmp->filename);
	}
}

/*
FILE		54		0-53
DIRECTORY	1		54
*/

int main(int argc, char *argv[])
{
	int status, i, fileOp;
	long msPause = 0;
	List fileList, directoryList;
	CSPRNG rng = NULL;
	init_list(&fileList);
	init_list(&directoryList);
	unsigned long randomDelay = 0;
	BOOL truRand = FALSE;

	if (argc > 1) {
		errno = 0;
		msPause = strtol(argv[1], NULL, 10);
		if (errno != 0) {
			printf("Error! Please enter only number values in ms.");
			return -1;
		}
	}

	if (msPause > 20000)
		msPause = 0;

	rng = csprng_create(rng);
	if (!rng)
		return 1;

	TCHAR base[100] = L"C:\\ProggerTests";
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\FullMultiOp");
	status = CreateDirectoryW(base, NULL);
	wcscat(base, L"\\");
	ListDirectoryFiles(base, &fileList, &directoryList);

	for (i = 0; i < 22000; i++) {
		if (msPause == -1) {
			randomDelay = csprng_get_int(rng);
			randomDelay %= 5001;
			Sleep(randomDelay);
			truRand = TRUE;
		}
		else
			Sleep(msPause);

		int myRand = rand() % 55;

		if (myRand >= 0 && myRand < 54)
			fileOperation(&fileList, base);
		else
			directoryOperation(&directoryList, base);
	}
	return 0;
}

